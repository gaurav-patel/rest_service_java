# README #

This RESTful service provides clients details on countries and cities in JSON response. The Service is built using Maven for dependency management, Jersey 2, Hibernate 5, MySQL, and running it on Glassfish 4, will optimize it to work on JBoss 6, and Tomcat 8.

## Planning
- /country resource (Completed)
- /city resource (Completed)
- Security - Probably OAuth
- HATEOAS - Probably will use for reference links to objects, like: capital city, cities for countries, and country for cities json response.
