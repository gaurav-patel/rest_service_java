package com.patel.gaurav.RESTfulService.Exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.patel.gaurav.RESTfulService.model.CustomError;

@Provider
public class CustomExceptionMapper implements ExceptionMapper<CustomException> {

	@Override
	public Response toResponse(CustomException ex) {
		CustomError e = new CustomError();
		e.setMessage(ex.getMessage());
		e.setCode(ex.getCode());
		
		return Response.status(e.getCode())
				.entity(e)
				.build();
	}

}
