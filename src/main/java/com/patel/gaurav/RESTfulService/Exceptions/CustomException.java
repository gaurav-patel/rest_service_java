package com.patel.gaurav.RESTfulService.Exceptions;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CustomException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String Message;
	private int Code;
	private static String documentation = "localhost:8080/RESTfulService"; //for valid data inputs.
	
	public CustomException(String message, int code) {
		super();
		Message = message;
		Code = code;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public int getCode() {
		return Code;
	}

	public void setCode(int code) {
		Code = code;
	}

	public static String getDocumentation() {
		return documentation;
	}
	
}
