package com.patel.gaurav.RESTfulService.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CustomError {
	private String Message;
	private int Code;
	private String Documentation = "localhost:8080/RESTfulService"; //for valid data inputs.
	
	public CustomError() { 
		
	}
	
	public CustomError(String message, int code) {
		super();
		Message = message;
		Code = code;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public int getCode() {
		return Code;
	}

	public void setCode(int code) {
		Code = code;
	}

	public String getDocumentation() {
		return Documentation;
	}

	public void setDocumentation(String documentation) {
		Documentation = documentation;
	}
	
}
