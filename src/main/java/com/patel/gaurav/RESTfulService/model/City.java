package com.patel.gaurav.RESTfulService.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="city")
@XmlRootElement
public class City implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ID;
	private String Name;
	private String CountryCode;
	private String District;
	private long Population;
	
	public City() { 
		ID = 0;
		Name = null;
		CountryCode = null;
		District = null;
		Population = 0;
	}
	
	public long getID() {
		return ID;
	}

	public void setID(int iD) {
		if(iD > 0 && iD <= new Integer("99999999999"))
			ID = iD;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		if(name.length() < 36)
			Name = name;
	}

	public String getCountryCode() {
		return CountryCode;
	}

	public void setCountryCode(String countryCode) {
		if(countryCode.length() == 3)
			CountryCode = countryCode.toUpperCase();
	}

	public String getDistrict() {
		return District;
	}

	public void setDistrict(String district) {
		if(district.length() <= 20)	
			District = district;
	}

	public long getPopulation() {
		return Population;
	}

	public void setPopulation(long population) {
		if(population > 0 && population <= new Long("99999999999"))
			Population = population;
	}

	@Override
	public String toString() {
		return "City [ID=" + ID + ", Name=" + Name + ", CountryCode=" + CountryCode + ", District=" + District
				+ ", Population=" + Population + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((CountryCode == null) ? 0 : CountryCode.hashCode());
		result = prime * result + ((District == null) ? 0 : District.hashCode());
		result = prime * result + (int) (ID ^ (ID >>> 32));
		result = prime * result + ((Name == null) ? 0 : Name.hashCode());
		result = prime * result + (int) (Population ^ (Population >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		City other = (City) obj;
		if (CountryCode == null) {
			if (other.CountryCode != null)
				return false;
		} else if (!CountryCode.equals(other.CountryCode))
			return false;
		if (District == null) {
			if (other.District != null)
				return false;
		} else if (!District.equals(other.District))
			return false;
		if (ID != other.ID)
			return false;
		if (Name == null) {
			if (other.Name != null)
				return false;
		} else if (!Name.equals(other.Name))
			return false;
		if (Population != other.Population)
			return false;
		return true;
	}
	
}
