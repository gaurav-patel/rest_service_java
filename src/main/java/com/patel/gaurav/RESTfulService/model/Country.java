package com.patel.gaurav.RESTfulService.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="country")
@XmlRootElement
public class Country implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private String Code;
	private String Code2;
	private String Name;
	private String Continent;
	private String Region;
	private double SurfaceArea;
	@Column(nullable=true)
	private Integer IndepYear;
	private long Population;
	@Column(nullable=true)
	private Double LifeExpectancy;
	@Column(nullable=true)
	private Double GNP;
	@Column(nullable=true)
	private Double GNPOld;
	private String LocalName;
	private String GovernmentForm;
	private String HeadOfState;
	@Column(name="Capital", nullable=true)
	private Long Capital_id;
	
	public Country() {	
		Code = null;
		Code2 = null;
		Name = null;
		Continent = null;
		Region = null;
		IndepYear = null;
		LifeExpectancy = null;
		GNPOld = null;
		LocalName = null;
		GovernmentForm = null;
		HeadOfState = null;
		Capital_id = null;
	}
	
	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		if(!code.matches("[a-zA-Z ]*\\d+.*") && code.length() == 3)
			Code = code.toUpperCase();
	}

	public String getCode2() {
		return Code2;
	}

	public void setCode2(String code2) {
		if(!code2.matches("[a-zA-Z ]*\\d+.*") && code2.length() < 3)
			Code2 = code2.toUpperCase();
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		if(name.length() < 53)
			Name = name;
	}

	public String getContinent() {
		return Continent;
	}

	public void setContinent(String continent) {
		if(continent.equalsIgnoreCase("Asia") || continent.equalsIgnoreCase("Europe") || 
				continent.equalsIgnoreCase("North America") || continent.equalsIgnoreCase("South America") ||
				continent.equalsIgnoreCase("Africa") || continent.equalsIgnoreCase("Oceania") || 
				continent.equalsIgnoreCase("Antartica")) {
				
			Continent = continent;
		}
	}

	public String getRegion() {
		return Region;
	}

	public void setRegion(String region) {
		if(region.length() < 27)
			Region = region;
	}

	public double getSurfaceArea() {
		return SurfaceArea;
	}

	public void setSurfaceArea(double surfaceArea) {
		if(surfaceArea <= 99_999_999.99)
			SurfaceArea = surfaceArea;
	}

	public Integer getIndepYear() {
		return IndepYear;
	}

	public void setIndepYear(Integer indepYear) {
		if((indepYear > 0 && indepYear <= 999_999) || indepYear == null)
			IndepYear = indepYear;
	}

	public long getPopulation() {
		return Population;
	}

	public void setPopulation(long population) {
		if(population > 0 && population <= new Long("99999999999"))
			Population = population;
	}

	public Double getLifeExpectancy() {
		return LifeExpectancy;
	}

	public void setLifeExpectancy(Double lifeExpectancy) {
		if((lifeExpectancy > 0 && lifeExpectancy <= 99.9) || lifeExpectancy == null)
			LifeExpectancy = lifeExpectancy;
	}

	public Double getGNP() {
		return GNP;
	}

	public void setGNP(Double gNP) {
		if((gNP >= 0 && gNP <= new Double(99_999_999.99)) || gNP == null)
			GNP = gNP;
	}

	public Double getGNPOld() {
		return GNPOld;
	}

	public void setGNPOld(Double gNPOld) {
		if((gNPOld > 0 && gNPOld <= 99_999_999.99) || gNPOld == null)
			GNPOld = gNPOld;
	}

	public String getLocalName() {
		return LocalName;
	}

	public void setLocalName(String localName) {
		if(localName.length() <= 45)
			LocalName = localName;
	}

	public String getGovernmentForm() {
		return GovernmentForm;
	}

	public void setGovernmentForm(String governmentForm) {
		if(governmentForm.length() <= 45)
			GovernmentForm = governmentForm;
	}

	public String getHeadOfState() {
		return HeadOfState;
	}

	public void setHeadOfState(String headOfState) {
		if(headOfState.length() <= 60 || headOfState == null)
			HeadOfState = headOfState;
	}

	public long getCapital_id() {
		return Capital_id;
	}

	public void setCapital_id(Long capital_id) {
		if((capital_id > 0 && capital_id <= new Long("99999999999")) || capital_id == null)
		Capital_id = capital_id;
	}
	
	@Override
	public String toString() {
		return "Country [Code=" + Code + ", Code2=" + Code2 + ", Name=" + Name + ", Continent=" + Continent
				+ ", Region=" + Region + ", SurfaceArea=" + SurfaceArea + ", IndepYear=" + IndepYear + ", Population="
				+ Population + ", LifeExpectancy=" + LifeExpectancy + ", GNP=" + GNP + ", GNPOld=" + GNPOld
				+ ", LocalName=" + LocalName + ", GovernmentForm=" + GovernmentForm + ", HeadOfState=" + HeadOfState
				+ ", Capital_id=" + Capital_id + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Capital_id == null) ? 0 : Capital_id.hashCode());
		result = prime * result + ((Code == null) ? 0 : Code.hashCode());
		result = prime * result + ((Code2 == null) ? 0 : Code2.hashCode());
		result = prime * result + ((Continent == null) ? 0 : Continent.hashCode());
		result = prime * result + ((GNP == null) ? 0 : GNP.hashCode());
		result = prime * result + ((GNPOld == null) ? 0 : GNPOld.hashCode());
		result = prime * result + ((GovernmentForm == null) ? 0 : GovernmentForm.hashCode());
		result = prime * result + ((HeadOfState == null) ? 0 : HeadOfState.hashCode());
		result = prime * result + ((IndepYear == null) ? 0 : IndepYear.hashCode());
		result = prime * result + ((LifeExpectancy == null) ? 0 : LifeExpectancy.hashCode());
		result = prime * result + ((LocalName == null) ? 0 : LocalName.hashCode());
		result = prime * result + ((Name == null) ? 0 : Name.hashCode());
		result = prime * result + (int) (Population ^ (Population >>> 32));
		result = prime * result + ((Region == null) ? 0 : Region.hashCode());
		long temp;
		temp = Double.doubleToLongBits(SurfaceArea);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Country other = (Country) obj;
		if (Capital_id == null) {
			if (other.Capital_id != null)
				return false;
		} else if (!Capital_id.equals(other.Capital_id))
			return false;
		if (Code == null) {
			if (other.Code != null)
				return false;
		} else if (!Code.equals(other.Code))
			return false;
		if (Code2 == null) {
			if (other.Code2 != null)
				return false;
		} else if (!Code2.equals(other.Code2))
			return false;
		if (Continent == null) {
			if (other.Continent != null)
				return false;
		} else if (!Continent.equals(other.Continent))
			return false;
		if (GNP == null) {
			if (other.GNP != null)
				return false;
		} else if (!GNP.equals(other.GNP))
			return false;
		if (GNPOld == null) {
			if (other.GNPOld != null)
				return false;
		} else if (!GNPOld.equals(other.GNPOld))
			return false;
		if (GovernmentForm == null) {
			if (other.GovernmentForm != null)
				return false;
		} else if (!GovernmentForm.equals(other.GovernmentForm))
			return false;
		if (HeadOfState == null) {
			if (other.HeadOfState != null)
				return false;
		} else if (!HeadOfState.equals(other.HeadOfState))
			return false;
		if (IndepYear == null) {
			if (other.IndepYear != null)
				return false;
		} else if (!IndepYear.equals(other.IndepYear))
			return false;
		if (LifeExpectancy == null) {
			if (other.LifeExpectancy != null)
				return false;
		} else if (!LifeExpectancy.equals(other.LifeExpectancy))
			return false;
		if (LocalName == null) {
			if (other.LocalName != null)
				return false;
		} else if (!LocalName.equals(other.LocalName))
			return false;
		if (Name == null) {
			if (other.Name != null)
				return false;
		} else if (!Name.equals(other.Name))
			return false;
		if (Population != other.Population)
			return false;
		if (Region == null) {
			if (other.Region != null)
				return false;
		} else if (!Region.equals(other.Region))
			return false;
		if (Double.doubleToLongBits(SurfaceArea) != Double.doubleToLongBits(other.SurfaceArea))
			return false;
		return true;
	}
	
	
}
