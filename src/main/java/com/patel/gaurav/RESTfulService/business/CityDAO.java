package com.patel.gaurav.RESTfulService.business;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.patel.gaurav.RESTfulService.Exceptions.CustomException;
import com.patel.gaurav.RESTfulService.model.City;


public class CityDAO {
	private SessionFactory sessionfactory = HibernateUtil.getSessionFactory();
	
	public City getCity(String code, long id) {
		City city = null;
		Session session = null;
		
		try {
			session = sessionfactory.openSession();
			session.beginTransaction();

			city = (City) session.createQuery("from City c where c.CountryCode = :para1 and c.ID = :para2")
					.setParameter("para1", code)
					.setParameter("para2", id)
					.uniqueResult();
			session.getTransaction().commit();
		} catch (Exception e) {
			if(session != null) {
				session.getTransaction().rollback();
			}
			
			throw new CustomException(e.getCause().getMessage(), 400); //Throwing friendly custom exception for user.
			
		} finally {
			if(session != null) session.close();
		}

		if(city == null) {
			throw new CustomException("City not found which has '" + code + "' country code and '" + id + "' city id.", 204);
		}
		
		return city;
	}	

	public List<City> getAllCities(String code) {
		List<City> cities = null;
		Session session = null;
		
		try {
			session = sessionfactory.openSession();
			session.beginTransaction();
			
			cities = session.createQuery("from City c where c.CountryCode = :para")
					.setParameter("para", code)
					.list();
			session.getTransaction().commit();
		} catch (Exception e) {
			if(session != null) {
				session.getTransaction().rollback();
			}
			
			throw new CustomException(e.getCause().getMessage(), 400); //Throwing friendly custom exception for user.
			
		} finally {
			if(session != null) session.close();
		}
		
		return cities;
	}
	
	public City addCity(City city) {
		Session session = null;
		City c = null;
		
		try {
			session = sessionfactory.openSession();
			session.beginTransaction();
			session.persist(city);
			session.flush();
			
			session.getTransaction().commit();
			
			c = getCity(city.getCountryCode(), city.getID());
		} catch (HibernateException e) {
			if(session != null) {
				session.getTransaction().rollback();
			}
			
			throw new CustomException(e.getCause().getMessage(), 400); //Throwing friendly custom exception for user.
			
		} finally {
			if(session != null) session.close();
		}

		return c;
	}
	
	public boolean updateCity(City city) {
		Session session = null;
		boolean succeed = true;
		
		try {
			session = sessionfactory.openSession();
			session.beginTransaction();
			
			session.saveOrUpdate(city);
			session.flush();
			
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session != null) {
				session.getTransaction().rollback();
			}
			
			throw new CustomException(e.getCause().getMessage(), 400); //Throwing friendly custom exception for user.
			//succeed = false;

		} finally {
			if(session != null) session.close();
		}
		
		return succeed;
	}
	
	public boolean deleteCity(int id) {
		Session session = null;
		boolean succeed = true;
		
		try {
			session = sessionfactory.openSession();
			session.beginTransaction();
			session.delete(session.get(City.class, id));
			session.flush();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session != null) {
				session.getTransaction().rollback();
			}
			
			throw new CustomException(e.getCause().getMessage(), 400); //Throwing friendly custom exception for user.
			//succeed = false;
		} finally {
			if(session != null) session.close();
		}

		return succeed;
	}
}
