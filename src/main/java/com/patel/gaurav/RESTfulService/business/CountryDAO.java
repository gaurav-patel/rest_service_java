package com.patel.gaurav.RESTfulService.business;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.patel.gaurav.RESTfulService.Exceptions.CustomException;
import com.patel.gaurav.RESTfulService.model.Country;

public class CountryDAO {
	private SessionFactory sessionfactory = HibernateUtil.getSessionFactory();
	
	public Country getCountryByCode(String code) {
		Country country = null;
		Session session = null;
		
		try {
			session = sessionfactory.openSession();
			session.beginTransaction();

			country = (Country) session.createQuery("from Country c where c.Code = :para")
					.setParameter("para", code)
					.uniqueResult();
			session.getTransaction().commit();
		} catch (Exception e) {
			if(session != null) {
				session.getTransaction().rollback();
			}
			
			throw new CustomException(e.getCause().getMessage(), 400); //Throwing friendly custom exception for user.
		
		} finally {
			if(session != null) session.close();
		}
		
		if(country == null) {
			throw new CustomException("Country not which has " + code + " code.", 204);
		}

		return country;
	}
	
	public List<Country> getAllCountry() {
		List<Country> countries = null;
		Session session = null;
		
		try {
			session = sessionfactory.openSession();
			session.beginTransaction();
			
			countries = session.createCriteria(Country.class).list();
			session.getTransaction().commit();
		} catch (Exception e) {
			if(session != null) {
				session.getTransaction().rollback();
			}
			
			throw new CustomException(e.getCause().getMessage(), 400); //Throwing friendly custom exception for user.
			
		} finally {
			if(session != null) session.close();
		}
		
		if(countries.size() == 0) {
			throw new CustomException("Contries not found, there may be a problem!", 204);
		}
		
		return countries;
	}
	
	public List<Country> getAllCountriesInRegion(String region) {
		List<Country> countries = null;
		Session session = null;
		
		try {
			session = sessionfactory.openSession();
			session.beginTransaction();
			
			countries = session.createQuery("from Country c where c.Region = :para")
					.setParameter("para", region)
					.list();
			session.getTransaction().commit();
		} catch (Exception e) {
			if(session != null) {
				session.getTransaction().rollback();
			}
			
			throw new CustomException(e.getCause().getMessage(), 400); //Throwing friendly custom exception for user.
			
		} finally {
			if(session != null) session.close();
		}
		
		if(countries.size() == 0) {
			throw new CustomException("Countries not found which falls into '" + region + "' region.", 204);
		}
		
		return countries;
	}
	public Country addCountry(Country country) {
		Session session = null;
		Country c = null;
		try {
			session = sessionfactory.openSession();
			session.beginTransaction();

			session.persist(country);
			session.flush();
			
			c = session.get(Country.class, country.getCode());
			
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session != null) {
				session.getTransaction().rollback();
			}
			
			throw new CustomException(e.getCause().getMessage(), 400); //Throwing friendly custom exception for user.

		} finally {
			if(session != null) session.close();
		}
		
		return c;
	}
	
	public boolean updateCountry(Country country) {
		Session session = null;
		boolean succeed = true;
		
		try {
			session = sessionfactory.openSession();
			session.beginTransaction();
			
			session.saveOrUpdate(country);
			session.flush();
			
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session != null) {
				session.getTransaction().rollback();
			}
			
			throw new CustomException(e.getCause().getMessage(), 400); //Throwing friendly custom exception for user.
			//succeed = false;
		} finally {
			if(session != null) session.close();
		}
		
		return succeed;
	}
	
	public boolean deleteCountry(String code) {
		Session session = null;
		boolean succeed = true;
		
		try {
			session = sessionfactory.openSession();
			session.beginTransaction();

			session.delete(session.get(Country.class, code.toUpperCase()));
			session.flush();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session != null) {
				session.getTransaction().rollback();
			}
			
			throw new CustomException(e.getCause().getMessage(), 400); //Throwing friendly custom exception for user.
			//succeed = false;
		} finally {
			if(session != null) session.close();
		}

		return succeed;
	}
}
