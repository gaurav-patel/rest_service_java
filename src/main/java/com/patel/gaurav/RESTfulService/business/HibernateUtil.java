package com.patel.gaurav.RESTfulService.business;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	
	private static final SessionFactory sessionfactory;
	
	static {
		try {
			sessionfactory = new Configuration().configure().buildSessionFactory();
		} catch (Throwable e) {
			System.out.println("Inital Session Factory creation failed. " + e);
			throw new ExceptionInInitializerError(e);
		}
	}
	
	public static SessionFactory getSessionFactory() {
		return sessionfactory;
	}
}
