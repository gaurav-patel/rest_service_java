package com.patel.gaurav.RESTfulService.services;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;


import com.patel.gaurav.RESTfulService.business.CityDAO;
import com.patel.gaurav.RESTfulService.model.City;


@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CityResource {
	
	private CityDAO cityservice = new CityDAO();
	
	@GET
	public List<City> getCities(@PathParam("countryCode") String code) {
		return cityservice.getAllCities(code.toUpperCase());
	}
	
	@GET
	@Path("/{cityId}")
	public City getCity(@PathParam("countryCode") String code, @PathParam("cityId") long cityId) {
		return cityservice.getCity(code, cityId);
	}
	
	@POST
	public Response addCity(City city) {
		City c = cityservice.addCity(city);
		if(c.equals(city)){
			return Response.status(Status.CREATED)
					.entity(c)
					.build();		
		} else {
			return Response.status(Status.CONFLICT)
					.entity(c)
					.build();
		}
	}
	
	@PUT
	@Path("/{cityId}")
	public Response updateCity(@PathParam("countryCode") String code, 
				@PathParam("cityId") int cityId, City city) {
		city.setID(cityId);
		
		if(cityservice.updateCity(city)) {
			return Response.status(Status.ACCEPTED)
					.build();
		} else {
			return Response.status(Status.NOT_MODIFIED)
					.build();
		}
	}
	
	@DELETE
	@Path("/{cityId}")
	public Response deleteCity(@PathParam("cityId") int cityId) {
		
		if(cityservice.deleteCity(cityId)) {
			return Response.status(Status.OK)
					.build();
		} else {
			return Response.status(Status.BAD_REQUEST)
					.build();
		}
	}
	
}
