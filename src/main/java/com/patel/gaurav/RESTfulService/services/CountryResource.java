package com.patel.gaurav.RESTfulService.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.patel.gaurav.RESTfulService.business.CountryDAO;
import com.patel.gaurav.RESTfulService.model.Country;

@Path("/country")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CountryResource {
	
	private CountryDAO countryservice = new CountryDAO();
	
	@GET
	public List<Country> getAllCountries(@QueryParam("region") String region) {
		if(region != null && !region.equals(" ")) {
			return countryservice.getAllCountriesInRegion(region);
		} else {
			return countryservice.getAllCountry();	
		}
	}
	
	@GET
	@Path("/{countryCode}")
	public Country getCountry(@PathParam("countryCode") String code) {
		return countryservice.getCountryByCode(code.toUpperCase());
	}
	
	@POST
	public Response addCountry(Country country) {
		Country c = countryservice.addCountry(country);
		if(c.equals(country)){
			return Response.status(Status.CREATED)
					.entity(countryservice.getCountryByCode(country.getCode().toUpperCase()))
					.build();		
		} else {
			return Response.status(Status.CONFLICT)
					.entity(c)
					.build();
		}
	}
	
	@PUT
	@Path("/{countryCode}")
	public Response updateCountry(@PathParam("countryCode") String code, Country country) {
		country.setCode(code);
		
		if(countryservice.updateCountry(country)) {
			return Response.status(Status.ACCEPTED)
					.entity(country)
					.build();
		} else {
			return Response.status(Status.NOT_MODIFIED)
					.build();
		}

	}
	
	@DELETE
	@Path("/{countryCode}")
	public Response deleteCountry(@PathParam("countryCode") String code)  {
		if(countryservice.deleteCountry(code.toUpperCase())){
			return Response.status(Status.ACCEPTED)
					.build();
		} else {
			return Response.status(Status.BAD_REQUEST)
					.build();
		}
	}
	
	@Path("/{countryCode}/city")
	public CityResource getCityResource() {
		return new CityResource();
	}
}