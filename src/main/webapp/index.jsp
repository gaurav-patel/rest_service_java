<html>
<body>
    <h2>Jersey RESTful Web Application!</h2>
    <p><a href="webapi/myresource">Jersey resource</a>
    <p>Visit <a href="http://jersey.java.net">Project Jersey website</a>
    for more information on Jersey!
    </p>
    
    <br />
    <br />
	<h2>Data Requirements:</h2>
	
	<br />
	<h3>Country</h3>
	<table>
		<tr>
			<th>Variables</th>
			<th>Rules</th>
		</tr>
		<tr>
			<td>Code</td>
			<td>Must be 3 characters long, no number or symbols or signs, cannot be empty, and unique.</td>
		</tr>		
		<tr>
			<td>Name</td>
			<td>Maximum of 52 characters long and cannot be empty.</td>
		</tr>
		<tr>
			<td>Continent</td>
			<td>Must be one of following: Asia, Europe, North America, Africa, Oceania, Antarctica, South America</td>
		</tr>
		<tr>
			<td>Region</td>
			<td>Maximum of 26 characters long and cannot be empty.</td>
		</tr>
		<tr>
			<td>Surface Area</td>
			<td>Must be positive numbers, maximum of 8 digits before decimal place, and 2 digits after decimal place.</td>
		</tr>
		<tr>
			<td>Independence Year</td>
			<td>Must be positive number, maximum of 6 digits, no decimal digits, and can be null.</td>
		</tr>
		<tr>
			<td>Population</td>
			<td>Must be positive number, maximum of 11 digits, no decimal digits, and cannot be null. </td>
		</tr>
		<tr>
			<td>Life Expectancy</td>
			<td>Must be positive numbers, maximum of 2 digits before decimal point, 1 digit after decimal point, and can be null.</td>
		</tr>
		<tr>
			<td>GNP</td>
			<td>Must be positive number, maximum of 8 digits before decimal point, 2 digits after decimal point, and can be null.</td>
		</tr>
		<tr>
			<td>GNP Old</td>
			<td>Must be positive number, maximum of 8 digits before decimal point, 2 digits after decimal point, and can be null.</td>
		</tr>
		<tr>
			<td>Local Name</td>
			<td>Maximum of 45 characters long, no symbols, and cannot be null.</td>
		</tr>
		<tr>
			<td>Government Form</td>
			<td>Maximum of 45 characters long, no symbols, and cannot be null.</td>
		</tr>
		<tr>
			<td>Head of State</td>
			<td>Maximum of 60 characters long, no symbols, and can be null.</td>
		</tr>
		<tr>
			<td>Capital</td>
			<td>Must be positive number that refers to ID value of City, maximum of 11 digits, can be null.</td>
		</tr>
		<tr>
			<td>Code 2</td>
			<td>Must be maximum of 2 characters long, no symbols or signs, and cannot be null.</td>
		</tr>
	</table>
	
	<br />
	<h3>City</h3>
	<table>
		<tr>
			<th>Variables</th>
			<th>Rules</th>
		</tr>
		<tr>
			<td>ID</td>
			<td>Must be positive number, maximum of 11 digits, unique, and auto generated.</td>
		</tr>
		<tr>
			<td>Name</td>
			<td>Maximum of 35 characters long, no symbols, and cannot be null.</td>
		</tr>
		<tr>
			<td>Country Code</td>
			<td>Must be 3 characters that refers to Code value of Country, no symbols or signs, and cannot be null.</td>
		</tr>
		<tr>
			<td>District</td>
			<td>Maximum of 20 characters long, no symbols, and cannot be null.</td>
		</tr>
		<tr>
			<td>Population</td>
			<td>Must be positive number, maximum of 11 digits, no decimal digits, and cannot be null.</td>
		</tr>
	</table>	
</body>
</html>
